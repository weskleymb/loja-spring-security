package br.senai.rn.loja;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class GeradorDeSenha {

    public static void main(String[] args) {
        StringBuilder senha = new StringBuilder();
        senha
            .append("Senha: ")
            .append(new BCryptPasswordEncoder().encode("123"));
        System.out.println(senha.toString());
    }

}
